import '../styles/globals.css';
import '../styles/ckeditor.css';
import 'animate.css';
import 'bootstrap-4-grid';

import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
Router.events.on('routeChangeStart', () => NProgress.start()); Router.events.on('routeChangeComplete', () => NProgress.done()); Router.events.on('routeChangeError', () => NProgress.done());

import { context } from '@reatom/react';
import { createStore } from '@reatom/core';

import React from "react";
import { ThemeContextProvider } from '../components/contexts/ThemeContextProvider/component';
import { YMInitializer } from 'react-yandex-metrika';

const { Provider: StoreProvider } = context;

const MyApp = ({ Component, pageProps }) => {
  const store = createStore();

  return (
    <StoreProvider value={store}>
      <ThemeContextProvider>
        <YMInitializer accounts={[72197887]} />
        <Component {...pageProps} />
      </ThemeContextProvider>
    </StoreProvider>
  );
}

export default MyApp;
