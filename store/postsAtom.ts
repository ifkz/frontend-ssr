import { declareAction, declareAtom } from '@reatom/core';

export const changePosts = declareAction<Array<Object>>();
export const postsAtom = declareAtom([], on => [on(changePosts, (state, payload) => payload)]);
