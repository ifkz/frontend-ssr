import { declareAction, declareAtom } from '@reatom/core';
import { CheckNode } from '../components/organisms/CheckModule';

export const changeCheckNodes = declareAction<CheckNode[]>();
export const checkNodesAtom = declareAtom([], on => [on(changeCheckNodes, (state, payload) => payload)]);
