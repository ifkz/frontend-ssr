import axios from "axios";

export const apiBaseUrl = "https://api.ifkz.org/";

export const controller = axios.create({
  baseURL: apiBaseUrl,
  responseType: "json"
});
