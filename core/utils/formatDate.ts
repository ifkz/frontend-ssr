import moment from 'moment';

export const formatDate = (date) => moment(String(date)).format("DD.MM.YYYY hh:mm");
