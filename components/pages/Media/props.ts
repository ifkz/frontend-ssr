import { Post } from "../types/Post";
import { PostMeta } from '../types/PostMeta';
import locales from '../../../core/locales';

export type Media = {
  id: Number,
  title: string,
  image: {
    url: string
  },
  url: string
}

export type Props = {
  posts: Media[],
  postsMeta: PostMeta,
  language: typeof locales
}
