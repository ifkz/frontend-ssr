import Head from 'next/head';
import { apiBaseUrl, controller } from '../../../core/api';

import {
  Heading,
  PostCard,
  PostCardLarge,
  Container,
  Grid,
  GridItem, TitleHeader, MainTemplate, Pagination,
} from '../../index';
import { css } from '@emotion/react';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC, Fragment } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { useRouter } from 'next/router';
import { Post } from '../../molecules/Post';
import Media from 'react-media';

const gridHover = css`
  &:hover {
   ${PostCard}, ${PostCardLarge} {
    opacity: .5;
   }
  }
  ${PostCard}:hover, ${PostCardLarge}:hover {
    opacity: 1;
  }
`;

const Blog: FC<Props> = ({ posts, postsMeta, language }: Props) => {
  const router = useRouter();

  const handlePaginationClick = (number) => () => {
    router.push(`${router.pathname}?page=${number}`);
  }

  return (
    <div>
      <MainTemplate
        header={mock.header}
        footer={mock.footer}
      >
        <Head>
          <title>{language.news.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <Container>
            <TitleHeader className="mb-4" subtitle={`${postsMeta.count} ${language.cases.publications}`}>
              <Heading className="m-0" as="h3">
                {language.news.title}
              </Heading>
            </TitleHeader>
            <Grid className="mb-4 grid-responsive" columns={12} gap="20px" css={gridHover}>
              {posts && posts.map((n, i) =>
                <GridItem columns={i == 0 ? 12 : 4} key={i}>
                  {i == 0 ?
                    <Media queries={{
                      medium: "(max-width: 900px)",
                      large: "(min-width: 900px)"
                    }}>
                      {matches => (
                        <Fragment>
                          {matches.medium && <Post post={n} />}
                          {matches.large && <Post post={n} isLarge />}
                        </Fragment>
                      )}
                    </Media>
                    :
                    <Post post={n} />
                  }
                </GridItem>
              )}
            </Grid>
            <Pagination
              className="mb-5"
              basePath={router.pathname}
              perPage={postsMeta.per_page}
              count={postsMeta.count}
              onClick={handlePaginationClick}
              active={parseInt(typeof router.query.page === "string" ? router.query.page : "1") || 1}
            />

          </Container>
        </main>
      </MainTemplate>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  const pageNumber = ctx.query.page || 1;
  const res = await controller.get(`/news/all?page=${pageNumber}&lang=${language}`);
  const posts = await res.data.body;
  const meta = await res.data.meta;

  return { props: { posts, postsMeta: meta, language: locales[language] } }
}

export default Blog;
