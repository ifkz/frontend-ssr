import locales from '../../../core/locales';
import { AxiosResponse } from 'axios';

export type Props = {
  language: typeof locales;
  registryStats: {
    international: Number,
    internationalCyr: Number,
    national: Number,
  } & Pick<AxiosResponse<any>, 'data'>;
}
