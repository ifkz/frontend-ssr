import Head from 'next/head';

import {
  Button,
  Card,
  Container, Heading,
  MainTemplate,
  TitleHeader,
} from '../../index';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { useRouter } from 'next/router';
import { controller } from '../../../core/api';

import { Bar } from 'react-chartjs-2';

import { ChartCaptionList } from '../../molecules/ChartCaptionList/component';
import { firstChart, secondChart, thirdChart } from './data';

const Stats: FC<Props> = ({ registryStats, language }: Props) => {
  const router = useRouter();
  const chartOptions = {
    legend: {
        position: "bottom",
        align: "start",
        labels: {
          fontSize: 13,
          fontFamily: "Open Sans"
        }
    },
    scales: {
      yAxes: [{
        ticks: {
          min: 0
        }
      }]
    }
  };

  return (
    <div>
      <MainTemplate
        header={mock.header}
        footer={mock.footer}
      >
        <Head>
          <title>{language.graphs.switcher_statistics}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <Container>
            <TitleHeader
              className="mb-5"
              subtitle={
                <Button className="py-2" css={{ fontSize: 13 }} onClick={() => router.push('/registry')}>{language.graphs.switcher_registry}</Button>
              }
            >
              <Heading className="m-0" as="h2">{language.graphs.switcher_statistics}</Heading>
            </TitleHeader>
            <Heading className="mt-0" as="h3">{language.graphs.subtitle}</Heading>
            <div className="d-flex mt-3" css={{ height: 20, overflow: "hidden", borderRadius: 10 }}>
              <div css={{ width: `${registryStats.international}%`, background: "blue" }} />
              <div css={{ width: `${registryStats.internationalCyr}%`, background: "#ff6155" }} />
              <div css={{ width: `${registryStats.national}%`, background: "#4CAF50" }} />
            </div>
            <ChartCaptionList data={[
              {
                caption: language.graphs.international,
                subtitle: `(${registryStats.data.international} ${language.graphs.adresses})`,
                color: "blue"
              },
              {
                caption: language.graphs.international_cyr,
                subtitle: `(${registryStats.data.international_cyr} ${language.graphs.adresses})`,
                color: "#ff6155"
              },
              {
                caption: language.graphs.national,
                subtitle: `(${registryStats.data.national} ${language.graphs.adresses})`,
                color: "#4CAF50"
              }
             ]} />

            <Heading className="mb-1" as="h1">{language.graphs.reasons}</Heading>
            <Heading className="mt-0" as="h3">{language.graphs.law}</Heading>
            <Card className="mt-2 mb-5 pt-5">
              <Bar data={firstChart(language)} options={chartOptions} />
            </Card>

            <Heading className="mt-0" as="h3">{language.graphs.procurature}</Heading>
            <Card className="mt-2 mb-5 pt-5">
              <Bar data={secondChart(language)} options={chartOptions} />
            </Card>

            <Heading className="mt-0" as="h3">{language.graphs.permitted}</Heading>
            <Card className="mt-2 mb-4 pt-5">
              <Bar data={thirdChart(language)} options={chartOptions} />
            </Card>
          </Container>
        </main>
      </MainTemplate>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  const response = await controller.get(`/graph`);
  const registryStats = {
    data: response.data,
    international: (response.data.international/response.data.all)*100,
    internationalCyr: (response.data.international_cyr/response.data.all)*100,
    national: (response.data.national/response.data.all)*100,
  }

  return { props: { registryStats, language: locales[language] } }
}

export default Stats;
