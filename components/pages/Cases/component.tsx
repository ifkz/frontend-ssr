import Head from 'next/head';
import { apiBaseUrl, controller } from '../../../core/api';

import {
  Heading,
  AnchorButton,
  PostCard,
  PostCardLarge,
  PostCardAuthor,
  PostCardBody,
  PostCardCover,
  PostCardPublished,
  Container,
  Grid,
  GridItem, TitleHeader, MainTemplate, Pagination,
} from '../../index';
import moment from 'moment';
import { css } from '@emotion/react';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC, Fragment } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { useRouter } from 'next/router';
import { Post } from '../../molecules/Post';
import Media from 'react-media';

const columnPerRow = (id: number, postsLength: number): number => {
  if((postsLength%8 - (id%8+1) == postsLength%8-2) || (postsLength%8 - (id%8+1) == postsLength%8-3) || (postsLength%8 - (id%8+1) == postsLength%8-4)){
    return 4;
  }
  else if((postsLength%8 - (id%8+1) == postsLength%8-5) || (postsLength%8 - (id%8+1) == postsLength%8-6) || (postsLength%8 - (id%8+1) == postsLength%8-7) || (postsLength%8 - (id%8+1) == postsLength%8-8)){
    return 3;
  }
  return 12;
}

const gridHover = css`
  &:hover {
   ${PostCard}, ${PostCardLarge} {
    opacity: .5;
   }
  }
  ${PostCard}:hover, ${PostCardLarge}:hover {
    opacity: 1;
  }
`;

const Cases: FC<Props> = ({ posts, postsMeta, language }: Props) => {
  const router = useRouter();

  const handlePaginationClick = (number) => () => {
    router.push(`${router.pathname}?page=${number}`);
  }

  return (
    <div>
      <MainTemplate
        header={mock.header}
        footer={mock.footer}
      >
        <Head>
          <title>{language.cases.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <Container>
            <TitleHeader className="mb-4" subtitle={`${postsMeta.count} ${language.cases.publications}`}>
              <Heading className="m-0" as="h3">
                {language.cases.title}
              </Heading>
            </TitleHeader>
            <Grid className="mb-4 grid-responsive" columns={12} gap="20px" css={gridHover}>
              {posts && posts.map((n, i) =>
                <GridItem columns={columnPerRow(i, postsMeta.count)} key={i}>
                  {columnPerRow(i, postsMeta.count) === 12 ?
                    <Media queries={{
                      medium: "(max-width: 900px)",
                      large: "(min-width: 900px)"
                    }}>
                      {matches => (
                        <Fragment>
                          {matches.medium && <Post post={n} />}
                          {matches.large && <Post post={n} isLarge />}
                        </Fragment>
                      )}
                    </Media>
                    :
                    <Post post={n} />
                  }
                </GridItem>
              )}
            </Grid>
            <Pagination
              className="mb-5"
              basePath={router.pathname}
              perPage={postsMeta.per_page}
              count={postsMeta.count}
              onClick={handlePaginationClick}
              active={parseInt(typeof router.query.page === "string" ? router.query.page : "1")}
            />

          </Container>
        </main>
      </MainTemplate>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  const pageNumber = ctx.query.page || 1;
  const res = await controller.get(`/cases/all?page=${pageNumber}&lang=${language}`);
  const posts = await res.data.body;
  const meta = await res.data.meta;

  return { props: { posts, postsMeta: meta, language: locales[language] } }
}

export default Cases;
