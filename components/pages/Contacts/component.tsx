import Head from 'next/head';

import {
  Container, Grid, GridItem, Heading,
  MainTemplate, PostCardAuthor,
} from '../../index';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { Paragraph } from '../../atoms/Paragraph';

const Contacts: FC<Props> = ({ language }: Props) => (
  <div>
    <MainTemplate
      header={mock.header}
      footer={mock.footer}
    >
      <Head>
        <title>{language.contacts.title}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Container>
          <Heading as="h1">{language.contacts.title}</Heading>
          <Paragraph css={{ fontSize: 20, maxWidth: 400 }}>
            {language.contacts.description + " "}
            <a css={{ color: "blue" }} href="mailto:info@ifkz.org">info@ifkz.org</a>
          </Paragraph>
          <Heading as="h3" className="mb-5">{language.contacts.team}</Heading>
          <Grid  className="grid-responsive" css={{ marginBottom: 100 }} columns={3} gap="30px" >
            <GridItem css={{ textAlign: "center" }} columns={1}>
              <img className="w-100 mb-3" src="/elzhan.png" alt={language.contacts.yelzhan_name} />
              <Heading as="span" css={{ display: "block", fontSize: 20, fontWeight: 700 }}>{language.contacts.yelzhan_name}</Heading>
              <Paragraph className="m-0 p-0 mt-1" css={{ fontSize: 12, lineHeight: 1.5, opacity: .7 }}>{language.contacts.yelzhan_role}</Paragraph>
            </GridItem>
            <GridItem css={{ textAlign: "center" }} columns={1}>
              <img className="w-100 mb-3" src="/gulmira.png" alt={language.contacts.gulmira_name} />
              <Heading as="span" css={{ display: "block", fontSize: 20, fontWeight: 700 }}>{language.contacts.gulmira_name}</Heading>
              <Paragraph className="m-0 p-0 mt-1" css={{ fontSize: 12, lineHeight: 1.5, opacity: .7 }}>{language.contacts.gulmira_role}</Paragraph>
            </GridItem>
            <GridItem css={{ textAlign: "center" }} columns={1}>
              <img className="w-100 mb-3" src="/arshyn.png" alt={language.contacts.arshyn_name} />
              <Heading as="span" css={{ display: "block", fontSize: 20, fontWeight: 700 }}>{language.contacts.arshyn_name}</Heading>
              <Paragraph className="m-0 p-0 mt-1" css={{ fontSize: 12, lineHeight: 1.5, opacity: .7 }}>{language.contacts.arshyn_role}</Paragraph>
            </GridItem>
            <GridItem css={{ textAlign: "center" }} columns={1}>
              <img className="w-100 mb-3" src="/yernazar.jpg" alt={language.contacts.yernazar_name} css={{ borderRadius: "100%", border: "4px solid #39aefe" }} />
              <Heading as="span" css={{ display: "block", fontSize: 20, fontWeight: 700 }}>{language.contacts.yernazar_name}</Heading>
              <Paragraph className="m-0 p-0 mt-1" css={{ fontSize: 12, lineHeight: 1.5, opacity: .7 }}>{language.contacts.yernazar_role}</Paragraph>
            </GridItem>
            <GridItem css={{ textAlign: "center" }} columns={1}>
              <img className="w-100 mb-3" src="/anwar.jpeg" alt={language.contacts.anwar_name} css={{ borderRadius: "100%", border: "4px solid #39aefe" }} />
              <Heading as="span" css={{ display: "block", fontSize: 20, fontWeight: 700 }}>{language.contacts.anwar_name}</Heading>
              <Paragraph className="m-0 p-0 mt-1" css={{ fontSize: 12, lineHeight: 1.5, opacity: .7 }}>{language.contacts.anwar_role}</Paragraph>
            </GridItem>
          </Grid>
        </Container>
      </main>
    </MainTemplate>
  </div>
);

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  return { props: { language: locales[language] } }
}

export default Contacts;
