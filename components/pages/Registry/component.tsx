import Head from 'next/head';

import {
  Anchor,
  Button,
  Card,
  Container,
  Heading,
  MainTemplate,
  RegistryModule,
  TitleHeader,
} from '../../index';
import { GetServerSideProps } from 'next';
import { Props } from './props';
import React, { FC } from 'react';
import * as mock from '../../templates/MainTemplate/mock';
import locales from '../../../core/locales';
import { Paragraph } from '../../atoms/Paragraph';
import { useRouter } from 'next/router';

const Registry: FC<Props> = ({ language }: Props) => {
  const router = useRouter();

  return (
    <div>
      <MainTemplate
        header={mock.header}
        footer={mock.footer}
        isFixed
      >
        <Head>
          <title>{language.registry.title}</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>

        <main>
          <RegistryModule className="mb-3" language={language} />
          <Container>

            <TitleHeader
              className="mb-3"
              subtitle={
                <Button className="py-2" css={{ fontSize: 13 }} onClick={() => router.push('/stats')}>{language.graphs.switcher_statistics}</Button>
              }
            >
              <Heading className="m-0" as="h2">{language.registryview.title}</Heading>
            </TitleHeader>
            <Card className="mb-5">
              <Paragraph className="m-0" css={{ lineHeight: 1.5, "a": { color: "blue" } }}>
                {language.registryview.description_first}&nbsp;
                <Anchor href="https://qogam.gov.kz/complain">Qogam.gov.kz/complain</Anchor>&nbsp;
                {language.registryview.description_second}&nbsp;
                <Anchor href="https://telegra.ph">Telegra.ph</Anchor>,&nbsp;
                <Anchor href="https://protonmail.com">ProtonMail.com</Anchor>,&nbsp;
                <Anchor href="https://live.onedrive.com">live.onedrive.com</Anchor>&nbsp;
                {language.registryview.description_third}&nbsp;
                <Anchor href="https://islam.by">Islam.by</Anchor>,&nbsp;
                <Anchor href="https://soundcloud.com">SoundCloud.com</Anchor>,&nbsp;
                <Anchor href="https://ice-cream.by">Ice-Cream.by</Anchor>&nbsp;
                {language.registryview.anothers}
              </Paragraph>
            </Card>
          </Container>

        </main>
      </MainTemplate>
    </div>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async ctx => {
  const language = ctx.locale || ctx.defaultLocale;
  return { props: { language: locales[language] } }
}

export default Registry;
