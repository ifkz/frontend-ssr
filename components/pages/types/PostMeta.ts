export type PostMeta = {
  count?: number,
  per_page?: number
}
