export type Post = {
  id?: number,
  title?: string,
  content?: string,
  cover?: { url?: string },
  author?: string,
  created_at?: string,
  updated_at?: string,
  author_link?: string,
  description?: string,
  case?: boolean,
  published?: string,
  seo_title?: string,
  seo_description?: string,
  seo_keywords?: string
}
