import styled from '@emotion/styled';
import { createElement, FC } from 'react';

import { Props } from './props';

const AnchorButtonBase: FC<Props> = ({ children, ...rest }: Props) =>
  createElement('button', rest, children);

export const AnchorButton = styled(AnchorButtonBase)`
  background: transparent;
  outline: none;
  cursor: pointer;
  border: none;
  color: -webkit-link;
  font-size: inherit;
  font-family: inherit;
  text-align: left;
  padding: 0;
`;
