import styled from '@emotion/styled';

import { Props } from './props';

export const Container = styled.div<Props>`
  position: relative;
  margin: auto;
  @media (max-width: 900px) {
    padding: 0 15px;
  }
  ${({ mini }: Props) => (mini ? 'max-width: 800px' : 'max-width: 900px')}


`;
