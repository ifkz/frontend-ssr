import styled from '@emotion/styled';
import classNames from 'classnames';
import React, { FC } from 'react';

import { Container } from '../../atoms/Container';
import { Props } from './props';

const FooterAboutBase: FC<Props> = ({ className, children, ...rest }: Props) => (
  <div className={classNames(className, 'about')} {...rest}>
    <Container>
      <p className="about__paragraph">{children}</p>
    </Container>
  </div>
);

export const FooterAbout = styled(FooterAboutBase)`
  padding: 45px 0;
  color: #fff;
  .about__title {
    display: block;
    font-size: 23px;
    font-weight: 600;
  }
  .about__paragraph {
    margin-top: 7px;
    line-height: 1.5;
    font-size: 17px;
  }
`;
