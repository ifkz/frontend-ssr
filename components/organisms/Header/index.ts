import { LogoProps } from './libs/Logo';
import { Props } from './props';

export * from './component';
export { Logo as HeaderLogo } from './libs/Logo';

export type HeaderProps = Props;
export type HeaderLogoProps = LogoProps;
