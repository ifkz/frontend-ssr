import locales from '../../../core/locales';
import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  language: typeof locales;
};
