import { Props } from './props';
import { Record } from './types/Record';
import { RegistryFieldProps } from './libs/RegistryField';

export * from './component';
export * from './libs/RegistryField';

export type RegistryModuleProps = Props;
export type RegistryRecord = Record;
export type { RegistryFieldProps };
