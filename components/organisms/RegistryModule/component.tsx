import React, { FC, FormEvent, FormEventHandler, useState } from 'react';
import { Props } from './props';
import { Card } from '../../molecules/Card';
import { RegistryField } from './libs/RegistryField';
import { Button } from '../../atoms/Button';
import { Heading } from '../../atoms/Heading';
import { Paragraph } from '../../atoms/Paragraph';
import { Container } from '../../atoms/Container';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTheme } from '@emotion/react';
import { controller } from '../../../core/api';
import { Record } from './types/Record';
import { ResponseMessage, ResponseMessageString } from '../../molecules/ResponseMessage';
import { RegistryTable } from './libs/RegistryTable';

export const RegistryModule: FC<Props> = ({ language, ...rest }: Props) => {
  const theme = useTheme()
  const fieldDefaultStyles = { outline: "none" }
  const [searchQuery, setSearchQuery] = useState<string>()
  const [registryRecord, setRegistryRecord] = useState<Record>(null)
  const [registryRecords, setRegistryRecords] = useState<Record[]>(null)
  const [message, setMessage] = useState<ResponseMessageString>()

  const onFieldChange = (event: FormEvent<HTMLInputElement>) => {
    setSearchQuery(event.currentTarget.value);
  }

  const searchRegistry = () => {
    setMessage({ type: "loading" })
    controller
      .get(`/registry/find?url=${searchQuery}`)
      .then((res) => {
        setRegistryRecord(res.data.body.registry)
        setRegistryRecords(res.data.body.registry_search)
        setMessage(res.data.body.registry ? { type: "error", text: `${registryRecord.domain} ${language.registry.found}` } : { type: "success", text: `${searchQuery} ${language.registry.not_found}`})
      })
      .catch((err) => {
        console.error(err)
        setMessage({ type: "error", text: language.homecheck.warn})
      })
  }

  return (
    <div {...rest}>
      <div
        className="py-5"
        css={
          {
            backgroundImage: theme.theme == "dark" ? "linear-gradient(to top, #080e2bcf, rgba(31, 25, 37, 0.87)), url(/background.png)" : "linear-gradient(to top, rgba(33, 150, 243, 0.7), rgba(33, 150, 243, 0.9)), url(/background.png)",
            backgroundSize: "cover"
          }
        }
      >
        <Container>
          <Heading color="#fff" as="h1" css={{ fontSize: 30, maxWidth: 600, margin: "20px auto", textAlign: "center", marginBottom: 5, paddingTop: 100, marginTop: 0 }}>
            {language.registry.title}
          </Heading>
          <Paragraph css={{ color: "#fff", opacity: .8, textAlign: "center", marginBottom: 30 }}>
            {language.registry.description}
          </Paragraph>
          <Card {...(theme.theme == "light" ? ({ css: { background: "#fff" }}) : {})}>
            {message && (<ResponseMessage type={message.type || null}>{message.text}</ResponseMessage>)}
            <div className="d-flex">
              <RegistryField
                className="col mr-2 px-4"
                onChange={onFieldChange}
                {...(theme.theme == "light" ? ({ css: { background: theme.darkBg, ...fieldDefaultStyles }}) : fieldDefaultStyles)}
                placeholder={language.registry.search_placeholder}
              />
              <Button
                className="d-inline-block"
                css={{ fontSize: 13, padding: "20px 25px" }}
                onClick={searchRegistry}
              >
                <FontAwesomeIcon icon={faSearch} css={{ width: 13, transform: "translateY(2px)", marginRight: 12 }} />
                {language.registry.search_value}
              </Button>
            </div>
          </Card>
          {registryRecords && (
            <div css={{ overflowX: "scroll" }}>
              <RegistryTable language={language} registry={registryRecords} />
            </div>
          )}
        </Container>
      </div>
    </div>
  )
}
