import { FC } from 'react';
import { Props } from './props';
import classNames from 'classnames';
import { useTheme } from '@emotion/react';


export const RegistryField:FC<Props> = ({ className, ...rest }: Props) => {
  const theme = useTheme();
  return (
    <input
      className={classNames(className, 'd-block', 'w-100')}
      css={{
        borderRadius: 100,
        border: `2px solid ${theme.darkBg}`,
        fontSize: 16,
        fontWeight: 500,
        outline: "none"
      }}
      { ...rest }
    />
  );
}
