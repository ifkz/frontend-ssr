import { HTMLAttributes } from 'react';
import { Record } from '../../types/Record';
import locales from '../../../../../core/locales';

export type Props = HTMLAttributes<HTMLTableElement> & {
  registry?: Record[];
  language: typeof locales;
}
