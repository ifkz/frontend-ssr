import { FC } from 'react';
import { Props } from './props';
import styled from '@emotion/styled';
import { css } from '@emotion/react';

const RegistryTableBase: FC<Props> = ({ registry, language, ...rest }: Props) => {
  return (
    <table {...rest}>
      <tbody>
        <tr>
          <th>№</th>
          <th>{language.registry.domain}</th>
          <th>{language.registry.reason}</th>
          <th>{language.registry.organization}</th>
          <th>{language.registry.document}</th>
          <th>{language.registry.number}</th>
        </tr>
        {registry && registry.map((n, i) => (
          <tr key={i}>
            <th>{n.id || language.registry.unknown}</th>
            <th>{n.domain || language.registry.unknown}</th>
            <th>{n.reason || language.registry.unknown}</th>
            <th>{n.organization || language.registry.unknown}</th>
            <th>{n.doctype || language.registry.unknown}</th>
            <th>{n.number || language.registry.unknown}</th>
          </tr>
        ))}
      </tbody>
    </table>
);
}

export const RegistryTable = styled(RegistryTableBase)<Props>`
  ${({ theme }) => css`
    color: ${theme.textColor};
    text-align: left;
    width: 100%;
    padding-bottom: 15px;
    td, th {
      text-align: left;
      padding: 12px 5px;
      border-bottom: 1px dashed rgba(255,255,255,.15);
    }
    tr {
      transition: background 0.2s;
      &:hover {
        background: ${theme.darkBg};
      }
    }
  `}
`
