import { CheckResult } from '../../types/CheckResult';
import { CardProps } from '../../../../molecules/Card';
import locales from '../../../../../core/locales';

export type Props = CardProps & {
  result: CheckResult;
  language: typeof locales;
}
