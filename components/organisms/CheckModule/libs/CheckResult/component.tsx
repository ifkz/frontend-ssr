import React, { FC } from 'react';
import { Props } from './props';
import { Card } from '../../../../molecules/Card';
import { css } from '@emotion/react';

export const CheckResult: FC<Props> = ({ result, language, ...rest }: Props) => {
  const ipFilter = (value) => value.split(".").map((seg, index) => {
    return index > 1 ? "*".repeat(Math.ceil(Math.log10(Number(seg)))) : seg;
  }).join(".");

  const availableStyles = css`
    color: #32ff32;
    &::after {
      content: '';
      display: inline-block;
      height: 7px;
      width: 7px;
      background: radial-gradient(#07fd03, #266f19);
      border-radius: 100px;
      box-shadow: 0 0 9px;
      margin-left: 10px;
      transform: translateY(-1px);
      animation-name: blinking;
      animation-duration: 2s;
      animation-iteration-count: infinite;
    }
  `;

  const unavailableStyles = css`
    color: #ff4335;
    &::after {
      content: '';
      display: inline-block;
      height: 7px;
      width: 7px;
      background: radial-gradient(#fd1a03, #901b15);
      border-radius: 100px;
      box-shadow: 0 0 9px;
      margin-left: 10px;
      transform: translateY(-1px);
      animation-name: blinking;
      animation-duration: 2s;
      animation-iteration-count: infinite;
    }
  `;

  return (
    <Card {...rest}>
      <header className="d-flex align-items-center" css={{ borderBottom: "1px dashed rgba(255,255,255,.2)", paddingBottom: 10, marginBottom: 10 }}>
        <img className="mr-2" css={{ height: 15 }} src={`/locales/${result.node.country_code.toLowerCase()}.png`} alt={result.node.country_code} />
        <span css={{ fontSize: 13, fontWeight: 600 }}>{result.node.country_code}</span>
      </header>
      <div>
        <div className="d-flex justify-content-between align-items-center py-1" css={{ fontSize: 13}}>
          <span className="d-block">IP</span>
          <span className="d-block">{ipFilter(result.node.ip)}</span>
        </div>
        <div className="d-flex justify-content-between align-items-center py-1" css={{ fontSize: 13}}>
          <span className="d-block">ISP</span>
          <span className="d-block">{result.node.isp}</span>
        </div>
        <div className="d-flex justify-content-between align-items-center py-1" css={{ fontSize: 13}}>
          <span className="d-block">HTTP</span>
          <span className="d-block" css={result.HTTP ? availableStyles : unavailableStyles}>{result.HTTP ? language.popular.available : language.popular.unavailable}</span>
        </div>
        <div className="d-flex justify-content-between align-items-center py-1" css={{ fontSize: 13}}>
          <span className="d-block">HTTPS</span>
          <span className="d-block" css={result.HTTPS ? availableStyles : unavailableStyles}>{result.HTTPS ? language.popular.available : language.popular.unavailable}</span>
        </div>
      </div>
    </Card>
  );
}
