import { CheckResult } from './CheckResult';

export type CheckResults = Record<string, CheckResult>;
