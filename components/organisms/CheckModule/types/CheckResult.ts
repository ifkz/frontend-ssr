import { CheckNode } from './CheckNode';

export type CheckResult = {
  HTTP?: boolean;
  HTTPS?: boolean;
  node: CheckNode;
};
