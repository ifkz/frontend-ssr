import { Props } from './props';
import { CheckNode } from './types/CheckNode';

export * from './component';

export type CheckModuleProps = Props;
export type { CheckNode };
