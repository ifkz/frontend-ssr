import { HTMLAttributes } from "react";

export type Props = HTMLAttributes<HTMLUListElement> & {
    readonly data: {
        readonly color: string;
        readonly caption: string;
        readonly subtitle: string;
    }[]
}