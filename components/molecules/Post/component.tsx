import React, { FC } from 'react';
import { Props } from './props';
import { AnchorButton } from '../../atoms/AnchorButton';
import {
  PostCard,
  PostCardAuthor,
  PostCardBody,
  PostCardCover,
  PostCardPublished,
} from '../PostCard';
import { apiBaseUrl } from '../../../core/api';
import { Heading } from '../../atoms/Heading';
import classNames from 'classnames';
import { useRouter } from 'next/router';
import { useTheme } from '@emotion/react';
import { formatDate } from '../../../core';
import { PostCardLarge } from '../PostCardLarge';

export const Post: FC<Props> = ({ post, className, isLarge, ...rest }: Props) => {
  const router = useRouter();
  const theme = useTheme();

  return !isLarge ? (
    <PostCard className={classNames(className, 'h-100')} {...rest}>
      <AnchorButton
        className="d-block"
        css={{
          color: 'inherit',
          fontWeight: "inherit",
          fontSize: "inherit"
        }}
        onClick={() => router.push(`/post/${post.id}`)}
      >
        <PostCardCover url={apiBaseUrl + post.cover.url}/>
      </AnchorButton>
      <PostCardBody css={{ background: theme.darkBg }}>
        <Heading className="mt-0" as="span" css={
          {
            textTransform: "uppercase",
            display: "block",
            fontSize: 17,
            fontWeight: 700,
            lineHeight: 1.6
          }
        }>
          <AnchorButton
            className="d-block"
            css={{
              color: 'inherit',
              fontWeight: "inherit",
              fontSize: "inherit"
            }}
            onClick={() => router.push(`/post/${post.id}`)}
          >
            {post.title}
          </AnchorButton>
        </Heading>
        <div className="d-flex mt-3">
          <PostCardAuthor href={post.author_link}
                          css={{marginRight: 10, fontWeight: 700}}>
            {post.author}
          </PostCardAuthor>
          <PostCardPublished>{formatDate(post.published)}</PostCardPublished>
        </div>
      </PostCardBody>
    </PostCard>
  ) : (
    <AnchorButton
      className="d-block w-100"
      css={{
        color: 'inherit',
        fontWeight: "inherit",
        fontSize: "inherit"
      }}
      onClick={() => router.push(`/post/${post.id}`)}
    >
      <PostCardLarge className={className} cover={apiBaseUrl + post.cover.url} {...rest}>
        <PostCardBody className="pb-4">
          <Heading className="mt-0" as="span" css={
            {
              textTransform: "uppercase",
              display: "block",
              fontSize: 30,
              fontWeight: 700,
              lineHeight: 1.6
            }
          }>
            <AnchorButton
              className="d-block"
              css={{
                color: 'inherit',
                fontWeight: "inherit",
                fontSize: "inherit"
              }}
              onClick={() => router.push(`/post/${post.id}`)}
            >
              {post.title}
            </AnchorButton>
          </Heading>
          <div className="d-flex mt-2">
            <PostCardAuthor href={post.author_link}
                            css={{marginRight: 10, fontWeight: 700 }}>
              {post.author}
            </PostCardAuthor>
            <PostCardPublished>{formatDate(post.published)}</PostCardPublished>
          </div>
        </PostCardBody>
      </PostCardLarge>
    </AnchorButton>
  );
}
