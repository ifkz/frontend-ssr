import { PostCardProps } from '../PostCard';
import { Post } from '../../pages/types/Post';

export type Props = PostCardProps & {
  post?: Post;
  isLarge?: boolean;
}
