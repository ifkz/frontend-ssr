import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & Pick<HTMLImageElement, 'src' | 'alt'>;
