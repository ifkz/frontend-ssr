import { CardProps } from '../Card';

export type Props = CardProps & {
  type?: 'error' | 'warn' | 'success' | 'loading';
}
