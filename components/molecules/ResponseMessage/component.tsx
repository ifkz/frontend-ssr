import { Card } from '../Card';
import React, { FC } from 'react';
import classNames from 'classnames';
import { useTheme } from '@emotion/react';
import { Props } from './props';


export const ResponseMessage: FC<Props> = ({ children, className, type, ...rest }: Props) => {
  const theme = useTheme();
  const typeStyles = {
    "error": {
      background: "#c74949",
      border: 0,
      borderBottom: "3px solid #793838"
    },
    "warn": {
      background: "#dca80b",
      border: 0,
      borderBottom: "3px solid #886a0b"
    },
    "success": {
      background: "#28a745",
      border: 0,
      borderBottom: "3px solid #19692c"
    },
    "loading": {
      border: 0,
      padding: 0
    }
  }

  return (
    <Card className={classNames(className, "mb-3", "text-center")} css={type ? typeStyles[type] : { background: theme.darkBg, border: "2px dashed", fontWeight: 700 }} {...rest}>
      {type == "loading" ?
        (<img src="/load.gif" alt="loading" css={{ height: 40, filter: "brightness(10)" }} />) :
        children}
    </Card>
  )
}
