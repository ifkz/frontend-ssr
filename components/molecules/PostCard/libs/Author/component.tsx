import styled from '@emotion/styled';
import React, { FC } from 'react';

import { Anchor } from '../../../../atoms/Anchor';
import { AnchorButton } from '../../../../atoms/AnchorButton';
import { Props } from './props';

const AuthorBase: FC<Props> = ({ children, ...rest }: Props) => {
  const Component = 'href' in rest ? Anchor : AnchorButton;
  return (
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    <Component {...(rest as any)}>{children}</Component>
  );
};

export const Author = styled(AuthorBase)`
  text-transform: uppercase;
  display: inline-block;
  font-size: 10px;
  letter-spacing: 1px;
  font-weight: 600;
`;
