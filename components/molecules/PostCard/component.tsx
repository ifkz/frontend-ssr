import classNames from 'classnames';
import React, { FC } from 'react';

import { Card } from '../Card';
import { Props } from './props';
import styled from '@emotion/styled';

const PostCardBase: FC<Props> = ({ className, children, ...rest }: Props) => {
  return (
    <Card
      className={classNames(className, 'd-flex', 'flex-column', 'p-0')}
      {...rest}
    >
      {children}
    </Card>
  );
};

export const PostCard = styled(PostCardBase)<Props>`
  transition: transform 0.2s, opacity 0.2s;
  height: 100%;
  &:hover {
    transform: scale(1.05);
  }
`;
