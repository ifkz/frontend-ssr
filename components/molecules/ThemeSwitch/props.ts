import { ButtonHTMLAttributes, HTMLAttributes } from 'react';

export type Props = Omit<HTMLAttributes<HTMLDivElement>, 'onClick'> &
  Pick<ButtonHTMLAttributes<HTMLButtonElement>, 'onClick'> & {
    readonly isActive: boolean;
  };
