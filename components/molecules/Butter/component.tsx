import styled from '@emotion/styled';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';
import React, { FC } from 'react';

import { Props } from './props';

const ButterBase: FC<Props> = ({ className, theme: _a, children, ...rest }: Props) => (
  <div className={classNames(className, 'butter')} {...rest}>
    <FontAwesomeIcon icon={faBars} />
    <span className="butter__title">{children}</span>
  </div>
);

export const Butter = styled(ButterBase)<Pick<Props, 'theme'>>`
  cursor: pointer;
  display: flex;
  align-items: center;
  opacity: 0.8;
  color: ${({ theme: colors }) => colors.textColor};
  transition: opacity 0.2s, color 0.2s;
  &:hover {
    opacity: 1;
  }
  &:hover svg {
    transform: rotate(180deg);
  }
  .butter__title {
    font-weight: 600;
    text-transform: uppercase;
    margin-left: 10px;
  }
  svg {
    font-size: 20px;
    width: 13px;
    color: inherit;
    transition: transform 0.2s;
  }
`;
