import styled from '@emotion/styled';

import { AnchorButton } from '../../../../atoms/AnchorButton';
import { Props } from './props';

export const Item = styled(AnchorButton)<Props>`
  ${({ active, theme }) =>
  active
    ? `
    background: ${theme.accentBlue};
    color: #fff;
  `
    : `
    background: #3a3958;
    color: #fff;
  `}
  padding: 7px 15px;
  margin: 5px 0;
  border-radius: 10px;
  cursor: pointer;
  box-shadow: none;
  transition: background 0.2s, color 0.2s, transform 0.1s, box-shadow 0.2s;
  &:hover {
    background: ${({ theme }) => theme.accentBlueHover};
    transform: scale(1.2);
    box-shadow: 0 6px 12px rgba(0, 0, 0, 0.4);
  }
`;
