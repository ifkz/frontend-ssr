import { AnchorButtonProps } from '../../../../atoms/AnchorButton';
import { ColorProps } from '../../../../../core/colors';

export type Props = AnchorButtonProps & {
  readonly active: boolean;
  readonly theme?: ColorProps;
};
