import { Props } from './props';

export * from './component';

export type LocaleSelectProps = Props;
