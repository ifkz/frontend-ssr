import styled from '@emotion/styled';

import { Props } from './props';

export const Grid = styled.div<Props>`
  ${({ columns, gap }) => `
    display: grid;
    grid-template-columns: repeat(${columns}, minmax(auto, 1fr));
    ${gap && `grid-gap: ${gap};`}
  `}

  @media (max-width: 900px) {
    grid-template-columns: repeat(2, minmax(auto, 1fr));
  }

  @media (max-width: 600px) {
    grid-template-columns: repeat(1, minmax(auto, 1fr));
  }
`;
