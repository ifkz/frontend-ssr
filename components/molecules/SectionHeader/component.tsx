import styled from '@emotion/styled';
import { Props } from './props';
import { css } from '@emotion/react';

export const SectionHeader = styled.header<Props>`
  ${({ cover, theme }) => css`
    background-image: ${theme.theme === "light" ? "linear-gradient(to top, rgb(39 150 239 / 70%), rgb(21 102 167 / 90%))" : "linear-gradient(to top, #080e2bcf, rgba(31, 25, 37, 0.87))" }, url(${cover});
    background-size: cover;
    background-position: center;
    text-align: center;
  `}
`;
